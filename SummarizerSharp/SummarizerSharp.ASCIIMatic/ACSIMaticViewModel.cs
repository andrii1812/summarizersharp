using System;
using SummarizerSharp.ACSIMatic.Util;
using SummarizerSharp.Data;
using SummarizerSharp.Data.Core;

namespace SummarizerSharp.ACSIMatic
{
    public class ACSIMaticViewModel : ModuleBaseViewModel
    {
        private readonly ScoreComputer _scoreComputer;
        private ACSIMaticConfig Config => CurrentConfiguration as ACSIMaticConfig;

        public ACSIMaticViewModel(Module module) : base(module)
        {
            _scoreComputer = new ScoreComputer(Config);
        }

        protected override void EditingEnded()
        {
            base.EditingEnded();
            OnPropertyChanged(nameof(CompressionPercents));
            OnPropertyChanged(nameof(StaticChosen));
            OnPropertyChanged(nameof(AverageChosen));
            OnPropertyChanged(nameof(StaticFrequency));
            OnPropertyChanged(nameof(SpacingCoeficient));
            OnPropertyChanged(nameof(StartEndingUsage));
            OnPropertyChanged(nameof(BeforeScore));
            OnPropertyChanged(nameof(MediumFirstScore));
            OnPropertyChanged(nameof(MediumSecondScore));
            OnPropertyChanged(nameof(Total));
        }

        public double Compression
        {
            get { return Config.Compression; }
            set
            {
                Config.Compression = value;
                OnChange();
            }
        }

        public int CompressionPercents
        {
            get { return (int) (Compression*100); }
            set { Compression = ((double) value)/100; }
        }

        #region RepresentativeFrequency

        public bool AverageChosen
        {
            get { return Config.ClippingType == ClippingType.Average; }
            set
            {
                if (value != AverageChosen && value)
                {
                    Config.ClippingType = ClippingType.Average;
                    OnPropertyChanged(nameof(StaticChosen));
                    OnPropertyChanged(nameof(AverageChosen));
                    OnChange();
                }
            }
        }

        public bool StaticChosen
        {
            get { return Config.ClippingType == ClippingType.Static; }
            set
            {
                if (value != StaticChosen && value)
                {
                    Config.ClippingType = ClippingType.Static;
                    OnPropertyChanged(nameof(StaticChosen));
                    OnPropertyChanged(nameof(AverageChosen));
                    OnChange();
                }
            }
        }

        public double StaticFrequency
        {
            get { return Config.StaticRepresentiveFrequency; }
            set
            {
                Config.StaticRepresentiveFrequency = value;
                OnPropertyChanged(nameof(StaticFrequency));
                OnChange();
            }
        }

        #endregion

        public double SpacingCoeficient
        {
            get { return Config.SpacingCoeficient; }
            set
            {
                Config.SpacingCoeficient = value;
                OnPropertyChanged(nameof(MediumFirstScore));
                OnPropertyChanged(nameof(MediumSecondScore));
                OnPropertyChanged(nameof(Total));
                OnChange();
            }
        }

        public bool StartEndingUsage
        {
            get { return Config.IsStartEndUsedInScoring; }
            set
            {
                Config.IsStartEndUsedInScoring = value;
                OnPropertyChanged(nameof(BeforeScore));
                OnPropertyChanged(nameof(Total));
                OnChange();
            }
        }

        public double BeforeScore => Math.Round(StartEndingUsage ? Math.Pow(1.0 / SpacingCoeficient, 2) : 0, 3);

        public double MediumFirstScore => Math.Round(Math.Pow(1.0 / SpacingCoeficient, 2), 3);

        public double MediumSecondScore => Math.Round(Math.Pow(1.0 / SpacingCoeficient, 3), 3);

        public double Total
        {
            get
            {
                var value = _scoreComputer.GetScore(new[]
                    {false, false, true, false, false, true, false, false, false, true, false, false});

                return Math.Round(value, 3);
            }
        }
    }
}