﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SummarizerSharp.ACSIMatic.Util
{
    class RepresentativeFrequencyProvider
    {
        private readonly ClippingType _type;
        private readonly double _staticValue;

        public RepresentativeFrequencyProvider(ACSIMaticConfig config)
        {
            _type = config.ClippingType;
            _staticValue = config.StaticRepresentiveFrequency;
        }

        public double GetFrequency(Dictionary<string, int> frequencies)
        {
            switch (_type)
            {
                case ClippingType.Average:
                    return frequencies.Sum(x => x.Value) / (double)frequencies.Count;
                case ClippingType.Static:
                    return _staticValue;
                default:
                    throw new InvalidOperationException("unknown clipping type");
            }
        }
    }
}
