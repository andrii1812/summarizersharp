﻿using System;
using System.Collections.Generic;

namespace SummarizerSharp.ACSIMatic.Util
{
    internal class ScoreComputer
    {
        private readonly ACSIMaticConfig _configuration;

        public ScoreComputer(ACSIMaticConfig configuration)
        {
            _configuration = configuration;
        }

        public double GetScore(IEnumerable<bool> represantatives)
        {
            double res = 0;
            int spacing = 0;
            foreach (var token in represantatives)
            {
                if (token)
                {
                    if (spacing > 0)
                    {
                        res += Math.Pow(1 / _configuration.SpacingCoeficient, spacing);
                    }
                    spacing = 0;
                    res++;
                }
                else if (Math.Abs(res) > 0.0 || _configuration.IsStartEndUsedInScoring)
                {
                    spacing++;
                }
            }
            if (_configuration.IsStartEndUsedInScoring && spacing > 0)
            {
                res += Math.Pow(1 / _configuration.SpacingCoeficient, spacing);
            }
            return res;
        }
    }
}