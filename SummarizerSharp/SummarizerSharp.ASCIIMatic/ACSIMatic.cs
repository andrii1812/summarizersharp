﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SharpNL;
using SharpNL.SentenceDetector;
using SharpNL.Stemmer;
using SummarizerSharp.ACSIMatic.Util;
using SummarizerSharp.Data;
using SummarizerSharp.Data.Core;
using SummarizerSharp.Data.Util;

namespace SummarizerSharp.ACSIMatic
{
    public class ACSIMatic : Module
    {
        FrequencyCounter _freq;
        IStemmer _stemmer;
        private ACSIMaticConfig _configuration;
        private RepresentativeFrequencyProvider _freqProvider;
        private ScoreComputer _scoreComputer;

        public override void Configure(IPluginConfiguration config)
        {
            _configuration = config as ACSIMaticConfig;
            base.Configure(_configuration);

            var lang = (CultureInfo)ModuleLoader.GetGlobalParameter("Culture");
            _stemmer = StemmerFactory.Get(lang);
            _freq = new FrequencyCounter(_stemmer);
            _freqProvider = new RepresentativeFrequencyProvider(_configuration);
            _scoreComputer = new ScoreComputer(_configuration);
        }

        public override string Name => "ACSI-Matic";

        public override IProcessingResult Process(IProcessingInput input)
        {
            var tokens = input.Sentences.SelectMany(x => x.Tokens);
            var frequencies = _freq.GetFrequencies(tokens);

            var average = _freqProvider.GetFrequency(frequencies);

            var groups = frequencies.OrderByDescending(x => x.Value)
                                    .GroupBy( x => x.Value > average)
                                    .ToList();

            var representatives = groups[0].Select(x => x.Key).ToList();

            Dictionary<ISentence, double> scores = new Dictionary<ISentence, double>();
            foreach (var sent in input.Sentences)
            {
                var represantiveArray = GetRepresentatives(sent, representatives);
                scores.Add(sent, _scoreComputer.GetScore(represantiveArray));
            }
            var ordered = scores.OrderByDescending(x => x.Value)
                                .Take((int) (_configuration.Compression * input.Sentences.Count))
                                .ToDictionary(x => x.Key, x => x.Value);

            var sentList = ordered
                .Select(x => (Sentence)x.Key)
                .OrderBy(x => x.Start)
                .ToList();

            return new ProcessingResult(new Document(input.Language ?? input.Text, sentList));
        }

        public override IPluginConfiguration GetConfiguration()
        {
            return new ACSIMaticConfig
            {
                Compression = 0.5,
                ClippingType = ClippingType.Average,
                StaticRepresentiveFrequency = 0.3,
                SpacingCoeficient = 2
            };
        }

        public override Type GetViewModel()
        {
            return typeof(ACSIMaticViewModel);
        }

        public override Type GetUserControl()
        {
            return typeof (ACSIMaticUserControl);
        }

        public IEnumerable<bool> GetRepresentatives(ISentence sentence, IList<string> representativeWords)
        {
            return sentence
                .Stemmed(_stemmer)
                .Select(x => x.Lexeme)
                .Select(representativeWords.Contains);
        }
    }
}
