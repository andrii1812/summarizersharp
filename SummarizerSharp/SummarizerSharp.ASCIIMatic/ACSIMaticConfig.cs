﻿using System;
using SummarizerSharp.ACSIMatic.Util;
using SummarizerSharp.Data.Core;

namespace SummarizerSharp.ACSIMatic
{
    public class ACSIMaticConfig : IPluginConfiguration
    {
        public double Compression { get; set; }
        public ClippingType ClippingType { get; set; }
        public double StaticRepresentiveFrequency { get; set; }
        public double SpacingCoeficient { get; set; }
        public bool IsStartEndUsedInScoring { get; set; }

        public bool Validate()
        {
            return Compression > 0 && Compression < 1 &&
                SpacingCoeficient > 0 && 
                StaticRepresentiveFrequency >= 0 && StaticRepresentiveFrequency < 1 &&
                Math.Abs(SpacingCoeficient) > 0.001;
        }

        public object Clone() => 
            new ACSIMaticConfig
            {
                Compression = Compression,
                ClippingType = ClippingType,
                StaticRepresentiveFrequency = StaticRepresentiveFrequency,
                SpacingCoeficient = SpacingCoeficient,
                IsStartEndUsedInScoring = IsStartEndUsedInScoring
            };
    }
}
