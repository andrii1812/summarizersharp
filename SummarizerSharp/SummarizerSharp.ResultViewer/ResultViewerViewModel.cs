﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Linq;
using SummarizerSharp.Data;
using SummarizerSharp.Data.Util;
using SummarizerSharp.Data.Core;

namespace SummarizerSharp.ResultViewer
{
    public class ResultViewerViewModel : ModuleBaseViewModel
    {
        private ICommand _showResultCommand;
        private IList<QueueItem> _allModules;

        public ResultViewerViewModel(Module module) : base(module)
        {
            Config.Computed += ResultChanged;
        }

        public override IPluginConfiguration CurrentConfiguration
        {
            get { return base.CurrentConfiguration; }
            set
            {
                base.CurrentConfiguration = value;
                Config.Computed += ResultChanged;
            }
        }

        private ResultViewerConfig Config => CurrentConfiguration as ResultViewerConfig;

        public ICommand ShowResultCommand => _showResultCommand ?? (_showResultCommand = new Command(ShowResultCallback));

        public bool ResultComputed { get; set; }

        public ObservableCollection<MarkedSentence> Sentences => Config.Sentences;

        public IList<QueueItem> AllModules
        {
            get { return _allModules; }
            set
            {
                _allModules = value;
                Config.OriginalInputModule = InputModule;
                Configure();
            }
        }

        public QueueItem InputModule => AllModules?[StandardInputModuleIndex];

        public int StandardInputModuleIndex
        {
            get
            {
                return Config.OriginalInputModuleIndex;
            }
            set
            {
                Config.OriginalInputModuleIndex = value;
                Config.OriginalInputModule = InputModule;
                OnChange();
            }
        }
        private void ShowResultCallback()
        {
            if (!ResultComputed)
            {
                return;
            }

            var resultForm = new ResultForm {DataContext = this};

            resultForm.Show();
        }

        protected override void EditingEnded()
        {
            Config.Computed -= ResultChanged;
            Config.Computed += ResultChanged;
            base.EditingEnded();
        }

        private void ResultChanged(bool result)
        {
            ResultComputed = result;
            OnPropertyChanged(nameof(ResultComputed));
            OnPropertyChanged(nameof(Sentences));
        }
    }
}