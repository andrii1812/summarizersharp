﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using SummarizerSharp.Data;
using SummarizerSharp.Data.Core;

namespace SummarizerSharp.ResultViewer
{
    public class ResultViewerConfig : IPluginConfiguration
    {
        public event Action<bool> Computed;

        [XmlIgnore]
        public ObservableCollection<MarkedSentence> Sentences = new ObservableCollection<MarkedSentence>();

        public int OriginalInputModuleIndex { get; set; }

        [XmlIgnore]
        public QueueItem OriginalInputModule { get; set; }

        public object Clone()
        {
            return new ResultViewerConfig();
        }

        internal void ResultComputed(bool result)
        {
            Computed?.Invoke(result);
        }

        public bool Validate()
        {
            return OriginalInputModuleIndex > -1;
        }
    }
}