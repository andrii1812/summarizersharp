﻿using System.Collections.Generic;
using SharpNL;
using SharpNL.SentenceDetector;
using SharpNL.Tokenize;

namespace SummarizerSharp.ResultViewer
{
    public class MarkedSentence : Sentence
    {
        public bool Contained { get; set; }

        public MarkedSentence(int start, int end, IDocument document, bool contained) : base(start, end, document)
        {
            Contained = contained;
        }

        public MarkedSentence(int start, int end, List<Token> tokens, IDocument document, bool contained) : base(start, end, tokens, document)
        {
            Contained = contained;
        }
    }
}