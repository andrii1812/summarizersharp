using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;

namespace SummarizerSharp.ResultViewer
{
    public class SentenceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var sent = value as ObservableCollection<MarkedSentence>;
            var textBlock = new TextBlock();
            textBlock.TextWrapping = TextWrapping.Wrap;

            if (sent == null)
            {
                return textBlock;
            }

            foreach (var sentence in sent)
            {
                var text = new Run(sentence.Text + " ");

                if (sentence.Contained)
                {
                    text.Background = new SolidColorBrush(Color.FromRgb(255, 255, 0));
                }

                textBlock.Inlines.Add(text);
            }
            
            return textBlock;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}