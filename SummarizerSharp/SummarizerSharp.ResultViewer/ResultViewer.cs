﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SummarizerSharp.Data.Core;

namespace SummarizerSharp.ResultViewer
{
    public class ResultViewer : Module
    {
        private ResultViewerConfig _configuration;

        public override void Configure(IPluginConfiguration config)
        {
            _configuration = config as ResultViewerConfig;
            base.Configure(config);
        }

        public override IProcessingResult Process(IProcessingInput input)
        {
            var orginalInput = _configuration.OriginalInputModule;

            var originalText = orginalInput.Run(null).ToInput();

            if (originalText.Sentences == null)
            {
                throw new InvalidOperationException();
            }

            var originalSentences = originalText.Sentences;
            var summarizedText = input.Sentences;
            var summarizedTextStarts = summarizedText.Select(x => x.Text).ToList();

            _configuration.Sentences.Clear();
            foreach (var originalSentence in originalSentences)
            {
                _configuration.Sentences.Add(
                    new MarkedSentence(
                        originalSentence.Start, 
                        originalSentence.End, 
                        originalText.Document, 
                        summarizedTextStarts.Contains(originalSentence.Text)));
            }

            _configuration.ResultComputed(true);
            return input.ToResult();
        }

        public override IPluginConfiguration GetConfiguration()
        {
            return new ResultViewerConfig();
        }

        public override Type GetViewModel()
        {
            return typeof(ResultViewerViewModel);
        }

        public override Type GetUserControl()
        {
            return typeof(ResultViewerUserControl);
        }
    }
}
