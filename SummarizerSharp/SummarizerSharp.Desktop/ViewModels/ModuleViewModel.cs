﻿namespace SummarizerSharp.Desktop.ViewModels
{
    internal class ModuleViewModel
    {
        public ModuleViewModel(string key, string name)
        {
            Key = key;
            DisplayName = name;
        }

        public string Key { get; }
        public string DisplayName { get; }
    }
}