﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using SummarizerSharp.Data;
using SummarizerSharp.Data.Core;
using SummarizerSharp.Data.Tasks;
using SummarizerSharp.Data.Util;
using SummarizerSharp.Desktop.Properties;
using SummarizerSharp.Desktop.Util;

namespace SummarizerSharp.Desktop.ViewModels
{
    public class TabViewModel : BaseViewModel
    {
        public static readonly int NoSelection = -1;

        private readonly SaveManager _saveManager = new SaveManager();
        private int _selectedIndex;
        private string _tabCaption;

        public TabViewModel(string name)
        {
            TabCaption = name;
            SelectedIndex = NoSelection;
            IsSaved = true;
            CollectionChanged += () =>
            {
                IsSaved = false;
                OnPropertyChanged(nameof(TabCaptionWithMarker));
            };
            Items.CollectionChanged += (o, args) => { CollectionChanged?.Invoke(); };
        }

        public bool RemoveEnabled => _selectedIndex >= 0;
        public bool RunAllEnabled => Items.Count > 0;
        public bool RunFromIndexEnabled => RunAllEnabled && RemoveEnabled;

        public string TabCaptionWithMarker
        {
            get { return _tabCaption + (!IsSaved ? "*" : ""); }
            set { _tabCaption = value; }
        }

        public ObservableCollection<QueueItem> Items { get; set; } = new ObservableCollection<QueueItem>();

        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                _selectedIndex = value;
                OnPropertyChanged(nameof(SelectedIndex));
                OnPropertyChanged(nameof(RemoveEnabled));
                OnPropertyChanged(nameof(RunFromIndexEnabled));
            }
        }

        public bool IsSaved { get; set; }

        public string TabCaption
        {
            get { return _tabCaption; }
            set
            {
                _tabCaption = value;
                OnPropertyChanged(nameof(TabCaptionWithMarker));
            }
        }

        private event Action CollectionChanged;

        public void AddModule(Module module, IPluginConfiguration config = null)
        {
            var queueItem = new QueueItem(module);
            queueItem.ModelControlViewModel.ViewModelChanged += () => { CollectionChanged?.Invoke(); };

            if (config != null)
            {
                queueItem.Configuration = config;
                queueItem.ModelControlViewModel.Configure();
            }

            Items.Add(queueItem);
            OnPropertyChanged(nameof(RunAllEnabled));
            OnPropertyChanged(nameof(RunFromIndexEnabled));
        }

        public void MoveSelectionUp()
        {
            if (SelectedIndex == 0 || SelectedIndex == -1)
            {
                return;
            }
            var oldSelection = SelectedIndex;

            var tmp = Items[SelectedIndex - 1];
            Items[SelectedIndex - 1] = Items[SelectedIndex];
            Items[SelectedIndex] = tmp;
            SelectedIndex = oldSelection - 1;
        }

        public void MoveSelectionDown()
        {
            if (SelectedIndex == Items.Count - 1 || SelectedIndex == -1)
            {
                return;
            }
            var oldSelection = SelectedIndex;
            var tmp = Items[SelectedIndex + 1];
            Items[SelectedIndex + 1] = Items[SelectedIndex];
            Items[SelectedIndex] = tmp;
            SelectedIndex = oldSelection + 1;
        }

        public void RunAll()
        {
            if (Items.Any(x => !x.Configuration.Validate()))
            {
                ErrorHandler.AddError(Resources.TabViewModel_RunAll_invalid_configuration);
                return;
            }

            TaskManager.Instance.AddTask(new SummTaskRunAll(Items.ToList(), TabCaption));
        }

        public void Save(string fileName)
        {
            try
            {
                _saveManager.Save(fileName, Items, _tabCaption);
                IsSaved = true;
                OnPropertyChanged(nameof(TabCaptionWithMarker));
            }
            catch (Exception e)
            {
                ErrorHandler.AddError(e.Message);
            }
        }

        public void RunFromSelected()
        {
            if (SelectedIndex == -1)
            {
                return;
            }

            if (SelectedIndex == 0)
            {
                RunAll();
                return;
            }

            var previous = Items[SelectedIndex - 1];
            if (!previous.IsCached || previous.ModelControlViewModel.ConfigurationChanged)
            {
                ErrorHandler.AddError(Resources.TabViewModel_RunFromSelected_Not_Cached_Previous);
            }

            TaskManager.Instance.AddTask(new SummTaskFromIndex(Items.ToList(), TabCaption, SelectedIndex));
        }

        public void RemoveSelected()
        {
            if (SelectedIndex == -1)
            {
                return;
            }

            Items.RemoveAt(SelectedIndex);
            SelectedIndex = -1;
            OnPropertyChanged(nameof(RunAllEnabled));
            OnPropertyChanged(nameof(RunFromIndexEnabled));
        }
    }
}