﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using SummarizerSharp.Data;
using SummarizerSharp.Data.Tasks;
using SummarizerSharp.Data.Util;
using SummarizerSharp.Desktop.Properties;
using SummarizerSharp.Desktop.Util;
using SummarizerSharp.Desktop.Views;

namespace SummarizerSharp.Desktop.ViewModels
{
    internal class MainWindowVM : BaseViewModel
    {
        private readonly ModuleLoader _moduleLoader = ModuleLoaderManager.Instance;
        private readonly SaveManager _saveManager = new SaveManager();
        private ICommand _addModuleCommand;
        private ICommand _closeTabCommand;
        private ICommand _loadCommand;
        private ICommand _loadModuleCommand;
        private ICommand _moveDownCommand;
        private ICommand _moveUpCommand;
        private ICommand _newTabCommand;
        private ICommand _runAllCommand;
        private ICommand _runSelected;
        private ICommand _saveCommand;
        private ModuleViewModel _selectedModule;
        private TabViewModel _selectedTab;
        private ICommand _removeSelectedCommand;

        public MainWindowVM()
        {
            Modules = new ObservableCollection<ModuleViewModel>(
                _moduleLoader.Names.Select(x => new ModuleViewModel(x, _moduleLoader[x].Name)));
            AddTab("untitled1");
        }

        public TaskManager TaskManager => TaskManager.Instance;

        public ErrorHandler ErrorHandler { get; set; } = new ErrorHandler();

        public ObservableCollection<ModuleViewModel> Modules { get; }

        public ModuleViewModel SelectedModule
        {
            get { return _selectedModule; }
            set
            {
                _selectedModule = value;
                OnPropertyChanged(nameof(AddModuleEnabled));
            }
        }

        public ObservableCollection<TabViewModel> Tabs { get; } = new ObservableCollection<TabViewModel>();

        public TabViewModel SelectedTab
        {
            get { return _selectedTab; }
            set
            {
                _selectedTab = value;
                OnPropertyChanged(nameof(SelectedTab));
                OnPropertyChanged(nameof(UpDownEnabled));
            }
        }

        public ICommand NewTab => _newTabCommand ?? (_newTabCommand = new Command(AddNewTab));
        public ICommand CloseSelected => _closeTabCommand ?? (_closeTabCommand = new Command(CloseSelectedCallback));
        public ICommand AddModule => _addModuleCommand ?? (_addModuleCommand = new Command(AddModuleToQueue));
        public ICommand MoveUp => _moveUpCommand ?? (_moveUpCommand = new Command(MoveModuleUp));
        public ICommand MoveDown => _moveDownCommand ?? (_moveDownCommand = new Command(MoveModuleDown));

        public bool AddModuleEnabled => SelectedModule != null;
        public ICommand RunAll => _runAllCommand ?? (_runAllCommand = new Command(RunAllCallback));
        public ICommand Save => _saveCommand ?? (_saveCommand = new Command(SaveCallback));
        public ICommand Load => _loadCommand ?? (_loadCommand = new Command(LoadCallback));
        public ICommand LoadModule => _loadModuleCommand ?? (_loadModuleCommand = new Command(LoadModuleCallback));

        public bool UpDownEnabled => SelectedTab?.Items.Count > 1;

        public ICommand RunSelected => _runSelected ?? (_runSelected = new Command(RunSelectedCallback));

        public ICommand RemoveSelected
            => _removeSelectedCommand ?? (_removeSelectedCommand = new Command(RemoveSelectedCallback));

        private void RemoveSelectedCallback()
        {
            SelectedTab?.RemoveSelected();
            OnPropertyChanged(nameof(UpDownEnabled));
        }

        private void RunSelectedCallback()
        {
            SelectedTab?.RunFromSelected();
        }

        private void CloseSelectedCallback()
        {
            if (Tabs.Count == 1)
            {
                ErrorHandler.AddError(Resources.MainWindowVM_CloseSelectedCallback_Unable_to_close_last_tab);
                return;
            }

            if (!SelectedTab.IsSaved)
            {
                var result = MessageBox.Show(
                    Resources.MainWindowVM_CloseSelectedCallback_SaveChanges,
                    Resources.MainWindowVM_CloseSelectedCallback_SaveChangesCaption,
                    MessageBoxButton.YesNoCancel,
                    MessageBoxImage.Question);
                if (result == MessageBoxResult.Cancel)
                {
                    return;
                }
                if (result == MessageBoxResult.Yes)
                {
                    SaveCallback();
                }
            }
            Tabs.Remove(SelectedTab);
            OnPropertyChanged(nameof(SelectedTab));
        }

        private void MoveModuleUp()
        {
            SelectedTab?.MoveSelectionUp();
        }

        private void MoveModuleDown()
        {
            SelectedTab?.MoveSelectionDown();
        }

        private void LoadModuleCallback()
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "zipped module|*.zip;";

            var result = dialog.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return;
            }

            try
            {
                var module = _moduleLoader.LoadCustomerModule(dialog.FileName);
                Modules.Add(new ModuleViewModel(module.Key, module.Name));
            }
            catch (InvalidOperationException e)
            {
                ErrorHandler.AddError(e.Message);
            }
        }

        private void SaveCallback()
        {
            var dialog = new SaveFileDialog();

            var result = dialog.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return;
            }

            var name = Path.GetFileNameWithoutExtension(dialog.FileName);
            SelectedTab.Save(dialog.FileName);
            SelectedTab.TabCaption = name;
        }

        private void RunAllCallback()
        {
            SelectedTab?.RunAll();
        }

        private void LoadCallback()
        {
            var dialog = new OpenFileDialog();

            var result = dialog.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return;
            }

            var tab = _saveManager.Load(dialog.FileName);
            tab.IsSaved = true;
            Tabs.Add(tab);
            SelectedTab = tab;
        }

        private void AddModuleToQueue()
        {
            SelectedTab.AddModule(_moduleLoader[SelectedModule.Key]);
            OnPropertyChanged(nameof(SelectedTab));
            OnPropertyChanged(nameof(UpDownEnabled));
        }

        private void AddNewTab()
        {
            var dialog = new NameInputValue();
            dialog.ResponseText = Resources.MainWindowVM_AddNewTab_DefaultWorksheetName;
            var result = dialog.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return;
            }

            AddTab(dialog.ResponseText);
        }

        private void AddTab(string name)
        {
            var tab = new TabViewModel(name);
            Tabs.Add(tab);
            SelectedTab = tab;
        }
    }
}