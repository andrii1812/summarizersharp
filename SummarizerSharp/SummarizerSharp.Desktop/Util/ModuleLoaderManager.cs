﻿using System.Windows;
using SummarizerSharp.Data;

namespace SummarizerSharp.Desktop.Util
{
    internal class ModuleLoaderManager
    {
        public static ModuleLoader Instance { get; private set; }

        public void Create(string directory)
        {
            Instance = new ModuleLoader(directory).LoadDirectory();
        }

        public void LoadResources(ResourceDictionary res)
        {
            Instance.LoadResources(res);
        }
    }
}