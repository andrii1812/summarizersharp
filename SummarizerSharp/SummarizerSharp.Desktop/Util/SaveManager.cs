﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using SummarizerSharp.Data;
using SummarizerSharp.Data.Core;
using SummarizerSharp.Desktop.ViewModels;

namespace SummarizerSharp.Desktop.Util
{
    internal class SaveManager
    {
        private readonly ModuleLoader _moduleLoader = ModuleLoaderManager.Instance;

        public void Save(string fileName, IReadOnlyList<QueueItem> items, string tabCaption)
        {
            var saveItems = new SaveModelCollection(tabCaption);
            saveItems.AddRange(items.Select(x => new SaveModel(x.Module.Key, x.Configuration)));
            using (var writer = new StreamWriter(fileName))
            {
                var serializer = new XmlSerializer(saveItems.GetType(), _moduleLoader.ConfigTypes.ToArray());
                serializer.Serialize(writer, saveItems);
            }
        }

        public TabViewModel Load(string fileName)
        {
            using (var reader = new StreamReader(fileName))
            {
                var serializer = new XmlSerializer(typeof(SaveModelCollection), _moduleLoader.ConfigTypes.ToArray());
                var items = (SaveModelCollection) serializer.Deserialize(reader);

                var name = Path.GetFileNameWithoutExtension(fileName);
                var tabViewModel = new TabViewModel(name);
                foreach (var item in items)
                {
                    tabViewModel.AddModule(_moduleLoader[item.ModuleName], item.Configuration);
                }
                return tabViewModel;
            }
        }
    }

    public class SaveModelCollection : List<SaveModel>, IXmlSerializable
    {
        public SaveModelCollection()
        {
        }

        public SaveModelCollection(string tabCaption)
        {
            TabCaption = tabCaption;
        }

        public string TabCaption { get; set; }

        public XmlSchema GetSchema() => null;

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement("SaveModelCollection");
            while (reader.IsStartElement("SaveModel"))
            {
                var attr = reader.GetAttribute("PluginConfigType");

                Type type = null;
                foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
                {
                    type = a.GetTypes().FirstOrDefault(x => x.AssemblyQualifiedName == attr);
                    if (type != null)
                        break;
                }
                if (type == null)
                {
                    throw new InvalidOperationException("type is not found");
                }

                var serial = new XmlSerializer(type);
                var name = reader.GetAttribute("ModuleName");
                reader.ReadStartElement("SaveModel");
                var config = (IPluginConfiguration) serial.Deserialize(reader);
                Add(new SaveModel(name, config));
                reader.ReadEndElement();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (var model in this)
            {
                writer.WriteStartElement("SaveModel");
                writer.WriteAttributeString("ModuleName", model.ModuleName);
                writer.WriteAttributeString("PluginConfigType", model.Configuration.GetType().AssemblyQualifiedName);
                var xmlSerializer = new XmlSerializer(model.Configuration.GetType());
                xmlSerializer.Serialize(writer, model.Configuration);
                writer.WriteEndElement();
            }
        }
    }

    public class SaveModel
    {
        public SaveModel(string moduleName, IPluginConfiguration configuration)
        {
            ModuleName = moduleName;
            Configuration = configuration;
        }

        public IPluginConfiguration Configuration { get; set; }

        public string ModuleName { get; set; }
    }
}