﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Navigation;
using SummarizerSharp.ACSIMatic;
using SummarizerSharp.Data.Ukrainian;
using SummarizerSharp.Data.Util;
using SummarizerSharp.Desktop.Util;

namespace SummarizerSharp.Desktop
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            var creator = new ModuleLoaderManager();
            creator.Create("Modules");
            creator.LoadResources(Resources);
            ModuleLoaderManager.Instance.AddGlobalParameter("Culture", new CultureInfo("ua"));

            StemmerFactory.LoadStemmersFromOpenNLP();
            StemmerFactory.Register(CultureInfo.GetCultureInfo("ua"), UkrainianStemmer.Instance);

            //SynonymsProviderFactory.Register(CultureInfo.GetCultureInfo("ua"),
            //                                 new UkrainianSynonymProvider("ukr_syn_dictionary.txt").Load());
        }
    }
}
