﻿using System.Linq;
using SummarizerSharp.Data.Core;
using System.Text.RegularExpressions;

namespace SummarizerSharp.Data.Ukrainian
{
    public class UkrainianSynonymProvider : BaseSynonymProvider
    {
        const string pattern = @"";

        public UkrainianSynonymProvider(string filename): base(filename)
        { }

        protected override Synonym ParseString(string input)
        {
            var res = new Synonym();
            var split = input.Split(' ');

            res.BaseWord = split.FirstOrDefault();
            res.Synonyms = split.Skip(1)
                                .Select(x => Regex.Replace(x, @"[\,|\.]", "").ToUpper())
                                .ToList();
            return res;
        }
    }
}