﻿using SharpNL.Stemmer.Snowball;
using System.Collections.Generic;
using System.Linq;

namespace SummarizerSharp.Data.Ukrainian
{
    public class UkrainianStemmer : SnowballStemmer
    {
        public static UkrainianStemmer Instance
        {
            get
            {
                if (ins == null)
                {
                    ins = new UkrainianStemmer();
                }
                return ins;
            }
        }

        static UkrainianStemmer ins = null;

        private UkrainianStemmer()
        {
            wends = word_ends.OrderByDescending(x => x.Length);
        }

        IEnumerable<string> word_ends = @"
          а ам ами ах та
          в вав вавсь вався вала валась валася вали вались валися вало валось валося вати ватись ватися всь вся
          е еві ем ею
          є ємо ємось ємося ється єте єтесь єтеся єш єшся єю
          и ив ий ила или ило илося им ими имо имось имося ите итесь итеся ити ить иться их иш ишся
          й ймо ймось ймося йсь йся йте йтесь йтеся
          і ів ій ім імо ість істю іть
          ї
          ла лась лася ло лось лося ли лись лися
          о ові овував овувала овувати ого ої ок ом ому осте ості очка очкам очками очках очки очків очкові очком очку очок ою
          ти тись тися
          у ував увала увати
          ь
          ці
          ю юст юсь юся ють ються
          я ям ями ях
        ".Trim().Split(new char[] { ' ', '\n' });
        
        IEnumerable<string> wends;
        
        IEnumerable<string> skip_ends = new List<string>() { "ер", "ск" };

        Dictionary<string, string> change_endings = new Dictionary<string, string>() {
            { "аче" , "ак" },
            { "іче" , "ік" },
            { "йовував" , "йов" }, { "йовувала", "йов" }, { "йовувати", "йов" },
            { "ьовував" , "ьов" }, { "ьовувала", "ьов" }, { "ьовувати", "ьов" },
            { "цьовував" , "ц" }, { "цьовувала", "ц" }, { "цьовувати", "ц" },
            { "ядер" , "ядр" }
        };

        // words to skip
        IEnumerable<string> stable_exclusions = new List<string> {
          "баядер", "беатріче",
          "віче",
          "наче", "неначе",
          "одначе",
          "паче"
        };

        // words to replace
        Dictionary<string, string> exclusions = new Dictionary<string, string> {
            { "відер", "відр" },
            { "був", "бува" }
        };


        public override string Stem(string word)
        {
            // normalize word
            word = replaceStressedVowels(word);

            // don't change short words
            if (word.Length <= 2) return word;

            // check for unchanged exclusions
            if (stable_exclusions.Contains(word))
            {
                return word;
            }

            // check for replace exclusions
            if (exclusions.ContainsKey(word))
            {
                return exclusions[word];
            }

            // changing endings
            // TODO order endings by abc DESC
            foreach (var eow in change_endings.Keys.OrderBy(x => change_endings[x]))
            {
                if (word.EndsWith(eow))
                {
                    return word.Substring(0, word.Length - eow.Length) + change_endings[eow];
                }
            }

            // match for stable endings
            foreach (var eow in skip_ends)
            {
                if (word.EndsWith(eow))
                {
                    return word;
                }
            }

            // try simple trim
            foreach (var eow in wends)
            {
                if (word.EndsWith(eow))
                {
                    var trimmed = word.Substring(0, word.Length - eow.Length);
                    if (trimmed.Length > 2)
                    {
                        return trimmed;
                    }
                }
            }  

            return word;
        }

        string replaceStressedVowels(string word)
        {
            Dictionary<string, string> nagolos = new Dictionary<string, string> {
                { "а́", "а" },
                { "е́", "е" },
                { "є́", "є" },
                { "и́", "и" },
                { "і́", "і" },
                { "ї́", "ї" },
                { "о́", "о" },
                { "у́", "у" },
                { "ю́", "ю" },
                { "я́", "я" }
            };

            return string.Join("", word.ToLower().Select(x => nagolos.ContainsKey(x.ToString()) ? nagolos[x.ToString()] : x.ToString()));
        }
    }
}
