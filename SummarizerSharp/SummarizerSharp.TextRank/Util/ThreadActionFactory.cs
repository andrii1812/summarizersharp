﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SummarizerSharp.TextRank.Util
{
    class ThreadActionFactory
    {
        private readonly int _start;
        private readonly int _end;
        private readonly double[,] _matrix;

        public ThreadActionFactory(int start, int end, double[,] matrix)
        {
            _start = start;
            _end = end;
            _matrix = matrix;
        }

        public void Action(object param)
        {
            var sent = param as List<IEnumerable<string>>;

            if (sent == null)
            {
                throw new ArgumentException("sent");
            }

            for (var i = 0; i < sent.Count - 1; i += 2)
            {
                var c11 = Math.Log(sent[i].Count());
                var c12 = Math.Log(sent[i + 1].Count());
                for (var j = _start; j < _end; j++)
                {
                    if (i == j) continue;

                    var c2 = Math.Log(sent[j].Count());
                    _matrix[i, j] = sent[i].Intersect(sent[j]).Count() / (c11 + c2);

                    if (i + 1 == j) continue;

                    _matrix[i + 1, j] = sent[i + 1].Intersect(sent[j]).Count() / (c12 + c2);
                }
            }
        }
    }
}
