﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SummarizerSharp.TextRank.Util
{
    class SimilarityComputer
    {
        private readonly int _threads;
        private double[,] _matrix;

        public SimilarityComputer(int threads)
        {
            _threads = threads;
        }

        public double[,] Compute(List<IEnumerable<string>> sent)
        {
            _matrix = new double[sent.Count, sent.Count];

            var tasks = new Task[_threads];

            int start = 0, step, end = step = sent.Count / _threads;
            for (int i = 0; i < _threads; i++)
            {
                var factory = new ThreadActionFactory(start, end, _matrix);
                tasks[i] = Task.Factory.StartNew(factory.Action, sent);
                
                start = end + 1;
                var tmpStep = step;
                if (start + step * 2 > sent.Count)
                {
                    tmpStep = sent.Count - start + 1;
                }
                end += tmpStep;
            }

            Task.WaitAll(tasks);
            return _matrix;
        }
    }
}
