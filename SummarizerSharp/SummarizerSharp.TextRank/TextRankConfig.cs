﻿using SummarizerSharp.Data.Core;

namespace SummarizerSharp.TextRank
{
    public class TextRankConfig : IPluginConfiguration
    {
        public double Compression { get; set; }
        public int Iterations { get; set; }
        public int Threads { get; set; }
        public double DampingFactor { get; set; }

        public object Clone()
        {
            return new TextRankConfig
            {
                Compression = Compression,
                Iterations = Iterations,
                Threads = Threads,
                DampingFactor = DampingFactor
            };
        }

        public bool Validate()
        {
            return Compression > 0 && Compression < 1 &&
                   Iterations > 0 &&
                   Threads > 0;
        }
    }
}