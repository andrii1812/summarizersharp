﻿using SummarizerSharp.Data;
using SummarizerSharp.Data.Core;

namespace SummarizerSharp.TextRank
{
    public class TextRankViewModel : ModuleBaseViewModel
    {
        public TextRankViewModel(Module module) : base(module)
        {
        }

        private TextRankConfig Config => CurrentConfiguration as TextRankConfig;

        public int CompressionPercents
        {
            get { return (int)(Config.Compression * 100); }
            set
            {
                Config.Compression = (double)value / 100; 
                OnChange();
            }
        }

        public int Iterations
        {
            get { return Config.Iterations; }
            set
            {
                Config.Iterations = value;
                OnChange();
            }
        }

        public int Threads
        {
            get { return Config.Threads; }
            set
            {
                Config.Threads = value; 
                OnChange();
            }
        }

        public double DampingFactor
        {
            get { return Config.DampingFactor; }
            set
            {
                Config.DampingFactor = value;
                OnChange();
            }
        }

        protected override void EditingEnded()
        {
            base.EditingEnded();
            OnPropertyChanged(nameof(Iterations));
            OnPropertyChanged(nameof(CompressionPercents));
            OnPropertyChanged(nameof(Threads));
            OnPropertyChanged(nameof(DampingFactor));
        }
    }
}