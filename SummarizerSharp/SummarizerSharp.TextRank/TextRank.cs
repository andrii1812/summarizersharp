﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SharpNL;
using SharpNL.SentenceDetector;
using SharpNL.Stemmer;
using SummarizerSharp.Data;
using SummarizerSharp.Data.Core;
using SummarizerSharp.Data.Util;
using SummarizerSharp.TextRank.Util;

namespace SummarizerSharp.TextRank
{
    public class TextRank : Module
    {
        private IStemmer _stemmer;
        private SimilarityComputer _similarityComputer;
        private TextRankConfig _configuration;

        public override void Configure(IPluginConfiguration config)
        {
            _configuration = config as TextRankConfig;
            base.Configure(config);
            
            var language = (CultureInfo)ModuleLoader.GetGlobalParameter("Culture");
            _stemmer = StemmerFactory.Get(language);
            _similarityComputer = new SimilarityComputer(_configuration.Threads);
        }

        public override IProcessingResult Process(IProcessingInput input)
        {
            if (input == null)
            {
                throw new ArgumentException("input");
            }

            var sentences = input.Sentences
                .Select(x => x.Stemmed(_stemmer)
                              .Select(y => _stemmer.Stem(y.Lexeme)))
                .ToList();

            var matrix = _similarityComputer.Compute(sentences);

            double d = _configuration.DampingFactor;
            double[] weights = new double[sentences.Count];

            for (var iter = 0; iter < _configuration.Iterations; iter++)
            {
                for (int i = 0; i < weights.Length; i++)
                {
                    double sum = 0.0;

                    for (int j = 0; j < weights.Length; j++)
                    {
                        double local = 0.0, value = 0.0, weight = 1.0;
                        for (var k = 0; k < weights.Length; k++)
                        {
                            local += matrix[j, k];
                        }                        
                        if (weights[j] > 0)
                        {
                            weight = weights[j];
                        }
                        value = matrix[j, i]*weight;
                        if (local > 0)
                        {
                            value /= local;
                        }
                        sum += value;
                    }

                    weights[i] = 1 - d + d * sum;
                }
            }

            var topSentences = weights
                .Zip(input.Sentences, (x, y) => new KeyValuePair<double, ISentence>(x, y))
                .OrderByDescending(x => x.Key);

            var summary = topSentences
                .Take(Convert.ToInt32(topSentences.Count() * _configuration.Compression))
                .Select(x => x.Value)
                .OrderBy(x => x.Start)
                .Cast<Sentence>()
                .ToList();

            return new ProcessingResult(new Document(input.Language ?? input.Text, summary));
        }

        public override IPluginConfiguration GetConfiguration()
        {
            return new TextRankConfig
            {
                Compression = 0.5,
                Iterations = 20,
                Threads = 4,
                DampingFactor = 0.85
            };
        }

        public override Type GetViewModel()
        {
            return typeof(TextRankViewModel);
        }

        public override Type GetUserControl()
        {
            return typeof(TextRankUserControl);
        }
    }
}
