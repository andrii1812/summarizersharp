﻿using System.Collections.Generic;
using SharpNL;
using SharpNL.SentenceDetector;
using SummarizerSharp.Data.Core;

namespace SummarizerSharp.Data
{
    public class ProcessingInput: IProcessingInput
    {
        private readonly IDocument _document;

        public ProcessingInput(IDocument document, string stringData = null)
        {
            _document = document;
            StringData = stringData;
        }

        public Document Document => (Document)_document;

        public IProcessingResult ToResult()
        {
            return new ProcessingResult(_document);
        }

        public ITextFactory Factory => _document.Factory;

        public string Language => _document.Language;

        public IReadOnlyList<ISentence> Sentences
        {
            get
            {
                return _document.Sentences;
            }

            set
            {
                _document.Sentences = value;
            }
        }

        public string StringData { get; set; }

        public string Text => _document.Text;
    }
}
