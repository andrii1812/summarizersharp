﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Windows;
using Module = SummarizerSharp.Data.Core.Module;

namespace SummarizerSharp.Data
{
    public class ModuleLoader
    {
        private readonly string _directory;
        private readonly Dictionary<string, object> _globalParameters = new Dictionary<string, object>();
        private readonly Dictionary<string, Module> _modules = new Dictionary<string, Module>();

        public ModuleLoader(string modulesDirectory)
        {
            _directory = modulesDirectory;
        }

        public IEnumerable<string> Names => _modules.Keys;

        public Module this[string name] => _modules[name];

        public IEnumerable<Type> ConfigTypes =>
            _modules.Values.Select(x => x.GetConfiguration()?.GetType()).Where(x => x != null);

        public void AddGlobalParameter(string name, object param)
        {
            _globalParameters.Add(name, param);
        }

        public void RemoveGlobalParameter(string name)
        {
            _globalParameters.Remove(name);
        }

        public void AddModule(string key, Module module)
        {
            module.Key = key;
            _modules.Add(key, module);
        }

        public object GetGlobalParameter(string name)
        {
            return _globalParameters[name];
        }

        private bool IsModuleExists(string name)
        {
            return Directory.Exists(Path.Combine(_directory, name)) &&
                   File.Exists(Path.Combine(_directory, name, name + ".dll"));
        }

        private string GetModuleName(string name)
        {
            return Path.GetFullPath(Path.Combine(_directory, name, name + ".dll"));
        }

        private Module Load(string name, string path)
        {
            var DLL = Assembly.LoadFile(path);

            foreach (var type in DLL.GetExportedTypes())
            {
                if (type.IsSubclassOf(typeof(Module)))
                {
                    var moduleInstance = (Module) Activator.CreateInstance(type);
                    moduleInstance.ModuleLoader = this;
                    AddModule(name, moduleInstance);
                    return moduleInstance;
                }
            }
            throw new ArgumentException(string.Format("Module \"{0}\" not found", name));
        }

        public ModuleLoader LoadDirectory()
        {
            foreach (var dirname in Directory.GetDirectories(_directory))
            {
                var info = new DirectoryInfo(dirname);
                Load(info.Name, GetModuleName(info.Name));
            }
            return this;
        }

        public void LoadResources(ResourceDictionary resources)
        {
            foreach (var module in _modules.Values)
            {
                LoadModuleResources(module, resources);
            }
        }

        public void LoadModuleResources(Module module, ResourceDictionary resources)
        {
            module.LoadUserControl(resources);
        }

        public Module LoadCustomerModule(string fileName)
        {
            var name = Path.GetFileNameWithoutExtension(fileName);

            if (name == null)
            {
                throw new InvalidOperationException("Invalid module package provided");
            }

            if (_modules.ContainsKey(name))
            {
                throw new InvalidOperationException($"Module ${name} already exists");
            }
            try
            {
                ZipFile.ExtractToDirectory(fileName, Path.Combine(_directory, name));
                var module = Load(name, GetModuleName(name));
                LoadModuleResources(module, Application.Current.Resources);
                return module;
            }
            catch (Exception)
            {
                throw new InvalidOperationException("Invalid module package provided");
            }
        }
    }
}