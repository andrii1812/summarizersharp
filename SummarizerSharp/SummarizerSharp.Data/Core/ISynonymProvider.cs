﻿using System.Collections.Generic;
using System.IO;

namespace SummarizerSharp.Data.Core
{
    public abstract class BaseSynonymProvider
    {
        public List<Synonym> synonym_words = new List<Synonym>();

        private string filename;

        public BaseSynonymProvider(string fileName)
        {
            filename = fileName;
        }

        public BaseSynonymProvider Load()
        {
            using (StreamReader reader = new StreamReader(filename))
            {
                while(!reader.EndOfStream)
                    synonym_words.Add(ParseString(reader.ReadLine()));
            }
            return this;
        }

        protected abstract Synonym ParseString(string input);
    }
}