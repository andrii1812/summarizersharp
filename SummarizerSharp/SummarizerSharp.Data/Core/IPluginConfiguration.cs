﻿using System;
using System.Runtime.InteropServices.ComTypes;

namespace SummarizerSharp.Data.Core
{
    public interface IPluginConfiguration : ICloneable
    {
        bool Validate();
    }
}
