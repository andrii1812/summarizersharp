﻿using System.Collections.Generic;
using System.Globalization;

namespace SummarizerSharp.Data.Core
{
    public abstract class CultureInstanceFactory<T>
    {
        protected static Dictionary<CultureInfo, T> items = new Dictionary<CultureInfo, T>();

        public static T Get(CultureInfo language)
        {
            return items[language];
        }

        public static void Register(CultureInfo lang, T stemmer)
        {
            items.Add(lang, stemmer);
        }

        public static void Register(Dictionary<CultureInfo, T> stemmers)
        {
            foreach (var val in stemmers)
            {
                items.Add(val.Key, val.Value);
            }
        }
    }
}
