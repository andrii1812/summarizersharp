﻿using System.Collections.Generic;
using System.Linq;
using SharpNL.SentenceDetector;
using SharpNL.Stemmer;
using SharpNL.Tokenize;

namespace SummarizerSharp.Data.Core
{
    public static class ISentenceExtension
    {
        public static IReadOnlyList<IToken> SkipPunctuation(this ISentence sentence)
        {
            return sentence.Tokens.Where(IsPunctuation).ToList();
        }

        public static IReadOnlyList<IToken> Stemmed(this ISentence sentence, IStemmer stemmer)
        {
            return sentence.Tokens.Select(x => 
                    new Token(x.Sentence as Sentence, x.Start, x.End, stemmer.Stem(x.Lexeme))).ToList();
        }

        private static bool IsPunctuation(IToken token)
        {
            return !(token.Lexeme.StartsWith(".") ||
                   token.Lexeme.StartsWith(",") ||
                   token.Lexeme.StartsWith("!") ||
                   token.Lexeme.StartsWith("-") ||
                   token.Lexeme.StartsWith("?"));
        }
    }
}
