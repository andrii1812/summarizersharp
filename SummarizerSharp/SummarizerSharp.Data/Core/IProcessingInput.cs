﻿using SharpNL;

namespace SummarizerSharp.Data.Core
{
    public interface IProcessingInput : IDocument
    {
        string StringData { get; set; }

        Document Document { get; }

        IProcessingResult ToResult();
    }
}
