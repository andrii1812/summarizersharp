﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace SummarizerSharp.Data.Core
{
    public abstract class Module
    { 
        public ModuleLoader ModuleLoader { get; set; }

        protected string ModulePrefix => $"Modules\\{Name}";

        public virtual string Name => GetType().Name;
        public string Key { get; set; }

        public virtual void Configure(IPluginConfiguration config)
        {
            if (config == null || !config.Validate())
            {
                throw new InvalidOperationException("invalid configuration provided");
            }
        }

        public abstract IProcessingResult Process(IProcessingInput input);

        public abstract IPluginConfiguration GetConfiguration();

        public abstract Type GetViewModel();

        public abstract Type GetUserControl();

        public void LoadUserControl(ResourceDictionary resources)
        {
            var viewModelType = GetViewModel();
            if (viewModelType?.Namespace == null)
            {
                return;
            }

            var viewType = GetUserControl();
            if (viewType?.Namespace == null)
            {
                return;
            }

            const string xamlTemplate = "<DataTemplate DataType=\"{{x:Type vm:{0}}}\"><v:{1} /></DataTemplate>";
            var xaml = string.Format(xamlTemplate, viewModelType.Name, viewType.Name);

            var context = new ParserContext();

            context.XamlTypeMapper = new XamlTypeMapper(new string[0]);
            context.XamlTypeMapper.AddMappingProcessingInstruction("vm", viewModelType.Namespace, viewModelType.Assembly.FullName);
            context.XamlTypeMapper.AddMappingProcessingInstruction("v", viewType.Namespace, viewType.Assembly.FullName);

            context.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
            context.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");
            context.XmlnsDictionary.Add("vm", "vm");
            context.XmlnsDictionary.Add("v", "v");

            var template = (DataTemplate)XamlReader.Parse(xaml, context);

            if (template.DataTemplateKey == null)
            {
                return;
            }

            resources.Add(template.DataTemplateKey, template);
        }
    }
}
