﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummarizerSharp.Data.Core
{
    public class Synonym
    {
        public string BaseWord { get; set; }

        public List<string> Synonyms { get; set; }

        public bool isSynonym(string word)
        {
            return Synonyms.Contains(word);
        }
    }
}
