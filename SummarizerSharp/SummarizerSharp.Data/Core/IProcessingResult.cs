﻿using SharpNL;
using System.Collections.Generic;

namespace SummarizerSharp.Data.Core
{
    public interface IProcessingResult :IDocument
    {
        int Length { get; }

        List<string> AdditionalMetadata { get; }

        IProcessingInput ToInput();
    }
}
