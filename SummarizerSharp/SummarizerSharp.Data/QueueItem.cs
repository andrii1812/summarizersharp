﻿using System;
using SummarizerSharp.Data.Core;
using System.Diagnostics;

namespace SummarizerSharp.Data
{
    [Serializable]
    public class QueueItem : BaseViewModel
    {
        private IProcessingResult _cachedValue;

        public bool IsCached => _cachedValue != null;

        public QueueItem(Module module)
        {
            Module = module;
            ModelControlViewModel = (ModuleBaseViewModel)Activator.CreateInstance(Module.GetViewModel(), module);
            ModelControlViewModel.Configure();
        }

        public Module Module { get; }

        public ModuleBaseViewModel ModelControlViewModel { get; set; }

        public IPluginConfiguration Configuration
        {
            get { return ModelControlViewModel.CurrentConfiguration; }
            set { ModelControlViewModel.CurrentConfiguration = value; }
        }

        public string ModuleName => Module.Name;

        public IProcessingResult Run(IProcessingInput input, bool force = false, ProcessingMetadata metadata = null)
        {
            if (metadata != null)
            {
                metadata.ModuleName = ModuleName;
            }

            try
            {
                if (!force && !ModelControlViewModel.ConfigurationChanged && IsCached)
                {
                    return _cachedValue;
                }
                Stopwatch stopwatch = new Stopwatch();

                stopwatch.Start();
                var res = Module.Process(input);
                stopwatch.Stop();

                metadata.CurrentDocumentLength = res.Length;
                metadata.AdditionalMetadata = res.AdditionalMetadata;

                if (metadata != null)
                {
                    metadata.TimeSpent = stopwatch.ElapsedMilliseconds / 1000.0;
                }

                ModelControlViewModel.ResultStatus = true;
                _cachedValue = res;
                return res;
            }
            catch (Exception e)
            {
                ModelControlViewModel.ResultStatus = false;
                return null;
            }
        }
    }
}