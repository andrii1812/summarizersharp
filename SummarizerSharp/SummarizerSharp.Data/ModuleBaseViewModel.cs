﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;
using SummarizerSharp.Data.Core;
using SummarizerSharp.Data.Util;

namespace SummarizerSharp.Data
{
    public abstract class ModuleBaseViewModel : BaseViewModel
    {
        private Module _module;
        private IPluginConfiguration _oldConfiguration;
        private IPluginConfiguration _currentConfiguration;
        private bool _modelChanged;
        private ICommand _saveCommand;
        private ICommand _cancelCommand;
        private string _errorMessage;
        private bool? _resultStatus;
        private string _moduleName;

        public event Action ViewModelChanged;

        protected ModuleBaseViewModel(Module module)
        {
            _module = module;
            IPluginConfiguration defaultConfiguration = module.GetConfiguration();
            _oldConfiguration = (IPluginConfiguration)defaultConfiguration.Clone();
            _currentConfiguration = defaultConfiguration;
            _modelChanged = false;
        }

        public string ModuleName
        {
            get
            {
                if (_module != null)
                    _moduleName = _module.Name;
                return _moduleName;
            }
            set { _moduleName = value; }
        }

        public virtual IPluginConfiguration CurrentConfiguration
        {
            get { return _currentConfiguration; }
            set
            {
                OnPropertyChanged(nameof(CurrentConfiguration));
                _currentConfiguration = value;
            }
        }

        [XmlIgnore]
        public ICommand SaveCommand => _saveCommand ?? (_saveCommand = new Command(SaveConfiguration));

        [XmlIgnore]
        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new Command(CancelConfigurationChanges));

        public void Configure()
        {
            try
            {
                _module.Configure(CurrentConfiguration);
                ErrorMessage = null;
                OnPropertyChanged(nameof(ErrorMessage));
            }
            catch (InvalidOperationException e)
            {
                ErrorMessage = e.Message;
                OnPropertyChanged(nameof(ErrorMessage));
            }
        }

        public void SaveConfiguration()
        {
            if(ErrorMessage != null)
                return;

            _oldConfiguration = _currentConfiguration;
            _currentConfiguration = (IPluginConfiguration)_currentConfiguration.Clone();
            ModelChanged = false;
            ConfigurationChanged = true;
        }

        public bool ConfigurationChanged { get; set; }

        public void CancelConfigurationChanges()
        {
            CurrentConfiguration = _oldConfiguration;
            _oldConfiguration = (IPluginConfiguration)_oldConfiguration.Clone();
            ModelChanged = false;
            ErrorMessage = null;
            OnPropertyChanged(nameof(ErrorMessage));
            ConfigurationChanged = false;
        }

        public void OnChange()
        {
            ModelChanged = true;
        }

        protected virtual void EditingEnded()
        {
            Configure();
            OnPropertyChanged(nameof(ButtonsVisibitity));

            ViewModelChanged?.Invoke();
        }

        [XmlIgnore]
        public Visibility ErrorVisibility => ErrorMessage == null ? Visibility.Hidden : Visibility.Visible;

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                _errorMessage = value;
                OnPropertyChanged(nameof(ErrorVisibility));
                OnPropertyChanged(nameof(SaveButtonEnabled));
            }
        }

        [XmlIgnore]
        public bool SaveButtonEnabled => ErrorMessage == null;

        [XmlIgnore]
        public Visibility ButtonsVisibitity => ModelChanged ? Visibility.Visible : Visibility.Hidden;

        private bool ModelChanged
        {
            get { return _modelChanged; }
            set
            {
                _modelChanged = value;
                EditingEnded();
            }
        }

        [XmlIgnore]
        public Visibility ResultVisible => ResultStatus.HasValue ? Visibility.Visible : Visibility.Hidden;

        public bool? ResultStatus
        {
            get { return _resultStatus; }
            set
            {
                _resultStatus = value; 
                OnPropertyChanged(nameof(ResultVisible));
                OnPropertyChanged(nameof(ResultStatusImagePath));
            }
        }

        [XmlIgnore]
        public string ResultStatusImagePath =>
            ResultStatus.HasValue && ResultStatus.Value ? "Resources/ok.png" : "Resources/run_error.png";

        [XmlIgnore]
        public Module Module
        {
            get { return _module; }
            set { _module = value; }
        }
    }
}
