﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace SummarizerSharp.Data.Tasks
{
    public class TaskManager : BaseViewModel
    {
        private static readonly Lazy<TaskManager> _instance = new Lazy<TaskManager>(() => new TaskManager());
        private readonly object _lockObj = new object();

        private TaskManager()
        {
            Task.Run(() => Run());
        }

        public static TaskManager Instance => _instance.Value;

        public static int EmptyPendingTasksDelay { get; set; } = 500;

        public ObservableCollection<SummTask> InputQueue { get; } = new ObservableCollection<SummTask>();

        private async void Run()
        {
            while (true)
            {
                SummTask item;
                lock (_lockObj)
                {
                    item = InputQueue.FirstOrDefault(x => x.Status == ProcessingStatus.Pending);
                }

                if (item == null)
                {
                    await Task.Delay(EmptyPendingTasksDelay);
                    continue;
                }

                await item.Run();
            }
        }

        public void AddTask(SummTask task)
        {
            task.Status = ProcessingStatus.Pending;
            lock (_lockObj)
            {
                InputQueue.Add(task);
            }
        }
    }
}