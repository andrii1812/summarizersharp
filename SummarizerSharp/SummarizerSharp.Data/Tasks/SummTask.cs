﻿using Microsoft.Win32;
using SummarizerSharp.Data.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;

namespace SummarizerSharp.Data.Tasks
{
    public enum ProcessingStatus
    {
        Pending,
        Running,
        Finished,
        Error
    }

    public abstract class SummTask : BaseViewModel
    {
        public SummTask()
        {
            MetadataContainer.CollectionChanged += (s, e) => OnPropertyChanged(nameof(TotalSpent));
        }

        protected IReadOnlyList<QueueItem> ProcessingItems;
        public ObservableCollection<ProcessingMetadata> MetadataContainer { get; set; } =
            new ObservableCollection<ProcessingMetadata>();

        private ProcessingStatus _status;
        private ICommand _exportCommand;

        public ProcessingStatus Status
        {
            get { return _status; }
            set
            {
                _status = value; 
                OnPropertyChanged(nameof(Status));
            }
        }

        public string Name { get; }
        public double TotalSpent => MetadataContainer.Sum(x => x.TimeSpent);

        protected SummTask(List<QueueItem> processingItems, string name) : this()
        {
            BindingOperations.EnableCollectionSynchronization(MetadataContainer, new object());
            ProcessingItems = processingItems;
            Name = name;
        }

        public ICommand ExportToExcel => _exportCommand ?? (_exportCommand = new Command(ExportToExcelCallback));

        private void ExportToExcelCallback()
        {
            var dialog = new SaveFileDialog();

            var result = dialog.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return;
            }          

            string filename = dialog.FileName;
            var exporter = new ExcelExporter(filename);
            exporter.AddGeneralData(Name, TotalSpent, _status);
            exporter.AddProcessingData(MetadataContainer.ToList());
            exporter.Save();
        }

        protected abstract void RunInternal();

        public Task Run()
        {
            return Task.Factory.StartNew(() =>
            {
                Status = ProcessingStatus.Running;
                try
                {
                    RunInternal();
                    Status = ProcessingStatus.Finished;
                }
                catch (Exception e)
                {
                    ErrorHandler.AddError(e.Message);
                    Status = ProcessingStatus.Error;
                }
            });
        }
    }
}