﻿using System.Collections.Generic;
using SummarizerSharp.Data.Core;

namespace SummarizerSharp.Data.Tasks
{
    public class SummTaskRunAll : SummTask
    {
        public SummTaskRunAll(List<QueueItem> processingItems, string name) : base(processingItems, name)
        {
        }

        protected override void RunInternal()
        {
            foreach (QueueItem item in ProcessingItems)
            {
                item.ModelControlViewModel.ResultStatus = null;
            }
            IProcessingInput input = null;
            var index = 1;
            foreach (var queueItem in ProcessingItems)
            {
                queueItem.Module.Configure(queueItem.Configuration);
                var metadata = new ProcessingMetadata();
                var result = queueItem.Run(input, true, metadata);
                metadata.Id = index++;
                MetadataContainer.Add(metadata);
                input = result.ToInput();
            }
        }
    }
}