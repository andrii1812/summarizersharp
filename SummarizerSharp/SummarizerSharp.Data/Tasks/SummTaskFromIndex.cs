using System.Collections.Generic;
using System.Linq;

namespace SummarizerSharp.Data.Tasks
{
    public class SummTaskFromIndex : SummTask
    {
        private readonly int _index;

        public SummTaskFromIndex(List<QueueItem> processingItems, string name, int index) : base(processingItems, name)
        {
            _index = index;
        }

        protected override void RunInternal()
        {
            var input = ProcessingItems[_index - 1].Run(null); // get cahched result
            var modules = ProcessingItems.Skip(_index).Select(x =>
            {
                x.ModelControlViewModel.ResultStatus = null;
                return x;
            });
            var inp = input.ToInput();
            var ind = _index + 1;
            foreach (var module in modules)
            {
                module.Module.Configure(module.Configuration);
                var metadata = new ProcessingMetadata();
                var result = module.Run(inp, true, metadata);
                metadata.Id = ind++;
                MetadataContainer.Add(metadata);
                inp = result.ToInput();
            }
        }
    }
}