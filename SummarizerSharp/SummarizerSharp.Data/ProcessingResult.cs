﻿using System;
using System.Collections.Generic;
using SharpNL;
using SharpNL.SentenceDetector;
using SummarizerSharp.Data.Core;

namespace SummarizerSharp.Data
{
    public class ProcessingResult : IProcessingResult
    {
        readonly IDocument _document;

        public ProcessingResult(IDocument document)
        {
            _document = document;
            AdditionalMetadata = new List<string>();
        }

        public List<string> AdditionalMetadata { get; }

        public ITextFactory Factory => _document.Factory;

        public string Language => _document.Language;

        public int Length => _document.Text.Length;

        public IReadOnlyList<ISentence> Sentences
        {
            get
            {
                return _document.Sentences;
            }

            set
            {
                _document.Sentences = value;
            }
        }

        public string Text => _document.Text;

        public IProcessingInput ToInput()
        {
            return new ProcessingInput(_document);
        }
    }
}
