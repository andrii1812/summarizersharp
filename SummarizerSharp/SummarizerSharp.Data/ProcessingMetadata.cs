﻿using System.Collections.Generic;

namespace SummarizerSharp.Data
{
    public class ProcessingMetadata
    {
        public int Id { get; set; }
        public List<string> AdditionalMetadata { get; internal set; }
        public int CurrentDocumentLength { get; internal set; }
        public string ModuleName { get; internal set; }
        public double TimeSpent { get; internal set; }
    }
}