using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace SummarizerSharp.Data
{
    public partial class ModulePicker
    {
        public ModulePicker()
        {
            InitializeComponent();
        }

        [Bindable(true)]
        public IList<QueueItem> AllModules
        {
            get { return (IList<QueueItem>)GetValue(AllModulesProperty); }
            set { SetValue(AllModulesProperty, value); }
        }

        public static readonly DependencyProperty AllModulesProperty =
            DependencyProperty.Register("AllModules", typeof(IList<QueueItem>), typeof(ModulePicker), new FrameworkPropertyMetadata
            {
                BindsTwoWayByDefault = true
            });

        [Bindable(true)]
        public int ModuleIndex
        {
            get { return (int)GetValue(ModuleIndexProperty); }
            set
            {
                SetValue(ModuleIndexProperty, value);
            }
        }

        public static readonly DependencyProperty ModuleIndexProperty =
            DependencyProperty.Register("ModuleIndex", typeof(int), typeof(ModulePicker), new FrameworkPropertyMetadata
            {
                BindsTwoWayByDefault = true
            });

        private void ModulePicker_OnLoaded(object sender, RoutedEventArgs e)
        {
            AllModules = ComboBox.Items.SourceCollection as IList<QueueItem>;
        }
    }
}