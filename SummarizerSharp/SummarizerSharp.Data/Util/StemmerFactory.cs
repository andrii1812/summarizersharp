﻿
using SharpNL.Stemmer;
using System.Globalization;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using SummarizerSharp.Data.Core;

namespace SummarizerSharp.Data.Util
{
    public class StemmerFactory : CultureInstanceFactory<IStemmer>
    {
        public static void LoadStemmersFromOpenNLP()
        {
            var cult = CultureInfo.GetCultures(CultureTypes.NeutralCultures);
            var types = from asm in AppDomain.CurrentDomain.GetAssemblies()
                        from type in asm.GetTypes()
                        where type.Namespace == "SharpNL.Stemmer.Snowball"
                        select type;

            foreach (var type in types)
            {
                var full_name = type.Name;
                Match match = Regex.Match(full_name, @"([A-Za-z]*)Stemmer");

                if (!match.Success)
                    continue;

                var lang_name = match.Groups[1].Value;

                var culture = cult.FirstOrDefault(x => x.EnglishName.Contains(lang_name));

                if (culture == null)
                    continue;

                IStemmer instance = null;
                try
                {
                    instance = (IStemmer)type.GetProperty("Instance").GetValue(null);
                }
                catch(NullReferenceException)
                {
                    instance = (IStemmer)Activator.CreateInstance(type);
                }
                items.Add(culture, instance);
            }
        }
    }
}