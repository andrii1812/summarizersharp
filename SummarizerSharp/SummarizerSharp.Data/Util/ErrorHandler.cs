﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;

namespace SummarizerSharp.Data.Util
{
    public class ErrorHandler : BaseViewModel
    {
        private static readonly Queue<string> ErrorQueue = new Queue<string>();
        private string _currentMessage;

        public ErrorHandler()
        {
            Task.Run(() => Run());
        }

        public string CurrentMessage
        {
            get { return _currentMessage; }
            set
            {
                _currentMessage = value;
                OnPropertyChanged(nameof(CurrentMessage));
                OnPropertyChanged(nameof(IconVisible));
            }
        }

        private bool HasErrors => !string.IsNullOrEmpty(_currentMessage);

        public Visibility IconVisible => HasErrors ? Visibility.Visible : Visibility.Hidden;

        public static int EmptyQueueDelay { get; set; } = 1000;

        public static int CurrentMessageDelay { get; set; } = 5000;

        private async void Run()
        {
            while (true)
            {
                if (ErrorQueue.Count == 0)
                {
                    await Task.Delay(EmptyQueueDelay);
                    continue;
                }

                CurrentMessage = ErrorQueue.Dequeue();
                await Task.Delay(CurrentMessageDelay);
                CurrentMessage = "";
            }
        }

        public static void AddError(string error)
        {
            ErrorQueue.Enqueue(error);
        }
    }
}