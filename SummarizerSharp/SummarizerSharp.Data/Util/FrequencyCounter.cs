﻿using SharpNL.Stemmer;
using SharpNL.Tokenize;
using System.Collections.Generic;

namespace SummarizerSharp.Data.Util
{
    public class FrequencyCounter
    {
        IStemmer stemmer;

        public FrequencyCounter(IStemmer stemmer)
        {
            this.stemmer = stemmer;
        }

        public Dictionary<string, int> GetFrequencies(IEnumerable<IToken> input)
        {
            Dictionary<string, int> freq = new Dictionary<string, int>();

            foreach (var word in input)
            {
                var stem = stemmer.Stem(word.Lexeme);

                int num = 1;
                if (freq.ContainsKey(stem))
                    num = freq[stem] + 1;

                freq[stem] = num;
            }
            return freq;
        }
    }
}
