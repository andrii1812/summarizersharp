﻿using System;
using System.Collections.Generic;
using SummarizerSharp.Data.Tasks;
using GemBox.Spreadsheet;
using System.Data;
using System.Threading.Tasks;
using System.Windows;

namespace SummarizerSharp.Data.Util
{
    public class ExcelExporter
    {
        private string _filename;
        private ExcelFile _file;
        private ExcelWorksheet _ws;
        private double _totalSpent;

        public ExcelExporter(string filename)
        {
            _filename = filename;
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
            _file = new ExcelFile();            
        }

        internal void AddGeneralData(string name, double totalSpent, ProcessingStatus _status)
        {
            _ws = _file.Worksheets.Add(name);
            _ws.Cells[0, 0].Value = "Name";
            _ws.Cells[0, 1].Value = name;

            _totalSpent = totalSpent;
            _ws.Cells[1, 0].Value = "Total time spent";
            _ws.Cells[1, 1].Value = totalSpent;
            _ws.Cells[1, 1].Style.NumberFormat = "0.000";

            _ws.Cells[2, 0].Value = "Processing Status";
            _ws.Cells[2, 1].Value = _status.ToString();
        }

        internal void AddProcessingData(List<ProcessingMetadata> list)
        {
            if(_ws == null)
            {
                throw new InvalidOperationException("Run AddGeneralData method before");
            }

            DataTable dt = new DataTable();
            
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Time", typeof(double));
            dt.Columns.Add("Time/Total", typeof(double));
            dt.Columns.Add("Output Document Length", typeof(int));
            dt.Columns.Add("Length from Original", typeof(double));
            dt.Columns.Add("Additional Info", typeof(string));

            var originalLength = list[0].CurrentDocumentLength;

            foreach(var item in list)
            {
                dt.Rows.Add(
                    item.Id,
                    item.ModuleName,
                    item.TimeSpent,
                    item.TimeSpent / _totalSpent,
                    item.CurrentDocumentLength,
                    item.CurrentDocumentLength / originalLength,
                    string.Join("\n", item.AdditionalMetadata));
            }
            
            _ws.InsertDataTable(dt,
                new InsertDataTableOptions()
                {
                    ColumnHeaders = true,
                    StartRow = 4,
                });

            for(int i = 0; i < 7; i++)
            {
                _ws.Columns[i].AutoFit();
            }
        }

        internal void Save()
        {
            Task.Run(async () =>
            {
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        _file.Save(_filename);
                        MessageBox.Show("Successfully saved", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                        break;
                    }
                    catch (Exception e)
                    {
                        ErrorHandler.AddError(e.Message);
                        await Task.Delay(3000);
                    }
                }
            });
        }
    }
}
