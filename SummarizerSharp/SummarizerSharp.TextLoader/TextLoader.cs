﻿using System;
using System.Globalization;
using System.IO;
using SharpNL.Analyzer;
using SummarizerSharp.Data;
using SummarizerSharp.Data.Core;
using SummarizerSharp.TextLoader.Readers;
using Document = SharpNL.Document;
using TextReader = SummarizerSharp.TextLoader.Readers.Interfaces.TextReader;

namespace SummarizerSharp.TextLoader
{
    public class TextLoader : Module
    {
        private CultureInfo _lang;
        private TextReader _reader;
        private TextReaderConfig _configuration;

        public override void Configure(IPluginConfiguration config)
        {
            base.Configure(config);
            _configuration = config as TextReaderConfig;
            if (_configuration == null)
            {
                throw new ArgumentNullException();
            }
            try
            {
                var fileName = _configuration.FileName;
                var ext = Path.GetExtension(fileName);
                switch (ext)
                {
                    case ".txt":
                    case ".csv":
                    case ".tsv":
                        _reader = new TxtTextReader(_configuration.FileName);
                        break;
                    case ".doc":
                    case ".docx":
                        _reader = new WordTextReader(_configuration.FileName);
                        break;
                    case ".pdf":
                        _reader = new PdfTextReader(_configuration.FileName);
                        break;
                    default:
                        throw new Exception();
                }
            }
            catch (Exception)
            {
                throw new InvalidOperationException("bad filename provided");
            }

            _lang = (CultureInfo)ModuleLoader.GetGlobalParameter("Culture");
        }

        public override IProcessingResult Process(IProcessingInput input)
        {
            var analyzer = new AggregateAnalyzer {
                $"{ModulePrefix}\\Models\\en-sent.bin",
                $"{ModulePrefix}\\Models\\en-token.bin"
            };

            var str = _reader.Read(); 
            var doc = new Document(_lang.EnglishName, str);

            analyzer.Analyze(doc);

            return new ProcessingResult(doc);
        }

        public override IPluginConfiguration GetConfiguration()
        {
            return new TextReaderConfig
            {
                FileName = ""
            };
        }

        public override Type GetViewModel()
        {
            return typeof(TextReaderViewModel);
        }

        public override Type GetUserControl()
        {
            return typeof(TextReaderUserControl);
        }
    }
}
