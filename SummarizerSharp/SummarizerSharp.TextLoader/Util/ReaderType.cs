﻿namespace SummarizerSharp.TextLoader.Util
{
    public enum ReaderType
    {
        Txt,
        Docx,
        Pdf
    }
}