﻿using System.IO;
using System.Text;
using TextReader = SummarizerSharp.TextLoader.Readers.Interfaces.TextReader;

namespace SummarizerSharp.TextLoader.Readers
{
    class TxtTextReader : TextReader
    {
        public TxtTextReader(string filename) : base(filename)
        {
        }

        public override string Read()
        {
            var result = new StreamReader(Filename, Encoding.UTF8).ReadToEnd();
            return result;
        }
    }
}
