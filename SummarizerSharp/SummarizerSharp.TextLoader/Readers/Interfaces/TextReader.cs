﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummarizerSharp.TextLoader.Readers.Interfaces
{
    abstract class TextReader
    {
        protected TextReader(string filename)
        {
            Filename = filename;
        }

        protected string Filename { get; }

        public abstract string Read();
    }
}
