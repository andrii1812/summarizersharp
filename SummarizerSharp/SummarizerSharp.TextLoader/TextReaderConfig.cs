﻿using SummarizerSharp.Data.Core;

namespace SummarizerSharp.TextLoader
{
    public class TextReaderConfig : IPluginConfiguration
    {
        public object Clone()
        {
            return new TextReaderConfig
            {
                FileName = FileName
            };
        }

        public bool Validate()
        {
            return FileName != "";
        }

        public string FileName { get; set; }
    }
}