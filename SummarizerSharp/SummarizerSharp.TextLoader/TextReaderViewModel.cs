﻿using SummarizerSharp.Data;
using SummarizerSharp.Data.Core;

namespace SummarizerSharp.TextLoader
{
    public class TextReaderViewModel : ModuleBaseViewModel
    {
        private TextReaderConfig Config => CurrentConfiguration as TextReaderConfig;

        public string FileName
        {
            get { return Config.FileName; }
            set
            {
                Config.FileName = value;
                OnPropertyChanged(nameof(FileName));
                OnChange();
            }
        }

        protected override void EditingEnded()
        {
            OnPropertyChanged(nameof(FileName));
            base.EditingEnded();
        }

        public TextReaderViewModel(Module module) : base(module)
        {
        }
    }
}