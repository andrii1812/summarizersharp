﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace SummarizerSharp.TextLoader
{
    /// <summary>
    /// Interaction logic for TextReaderUserControl.xaml
    /// </summary>
    public partial class TextReaderUserControl : UserControl
    {
        public TextReaderUserControl()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            var result = dialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                var model = (TextReaderViewModel) DataContext;
                model.FileName = dialog.FileName;
            }
        }
    }
}
