﻿using System;
using System.Xml.Serialization;
using SummarizerSharp.Data;
using SummarizerSharp.Data.Core;

namespace SummarizerSharp.RougeN
{
    public class RougeNConfig : IPluginConfiguration
    {
        public event Action Computed; 

        public int NGramLength { get; set; }
        public int StandardInputModuleIndex { get; set; }

        [XmlIgnore]
        public QueueItem StandardInputModule { get; set; }

        [XmlIgnore]
        public double Result { get; set; }

        public object Clone()
        {
            var rougeNConfig = new RougeNConfig
            {
                NGramLength = NGramLength,
                StandardInputModuleIndex = StandardInputModuleIndex,
                StandardInputModule = StandardInputModule,
                
            };
            rougeNConfig.Computed = (Action)Computed?.Clone();
            return rougeNConfig;
        }

        internal void ResultComputed(double result)
        {
            Result = result;
            Computed?.Invoke();
        }

        public bool Validate()
        {
            return NGramLength >= 1;
        }
    }
}