﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SharpNL;
using SharpNL.NGram;
using SharpNL.SentenceDetector;
using SharpNL.Stemmer;
using SummarizerSharp.Data.Core;
using SummarizerSharp.Data.Util;

namespace SummarizerSharp.RougeN
{
    public class RougeN : Module
    {
        private RougeNConfig _configuration;
        private IStemmer _stemmer;
        public override string Name => "Rouge-N";

        public override void Configure(IPluginConfiguration config)
        {
            _configuration = config as RougeNConfig;
            base.Configure(config);

            var lang = (CultureInfo)ModuleLoader.GetGlobalParameter("Culture");
            _stemmer = StemmerFactory.Get(lang);
        }

        public override IProcessingResult Process(IProcessingInput input)
        {
            var standardInput = _configuration.StandardInputModule;

            var standardNGrams = GetNGrams(standardInput.Run(null).Sentences);
            var inputNGrams = GetNGrams(input.Sentences);

            var matches = inputNGrams.Intersect(standardNGrams);

            double result = matches.Count() / (double)standardNGrams.Count;

            _configuration.ResultComputed(result);
            var res = input.ToResult();
            res.AdditionalMetadata.Add(result.ToString());
            return res;
        }

        private List<string> GetNGrams(IReadOnlyList<ISentence> sentences)
        {
            var tokens = sentences
                .SelectMany(x => x.Stemmed(_stemmer).Select(y => y.Lexeme))
                .Distinct()
                .ToList();
            var nGrams = NGramGenerator.Generate(tokens, _configuration.NGramLength, " ");
            return nGrams;
        }

        public override IPluginConfiguration GetConfiguration()
        {
            return new RougeNConfig { NGramLength = 1};
        }

        public override Type GetViewModel()
        {
            return typeof(RougeNViewModel);
        }

        public override Type GetUserControl()
        {
            return typeof(RougeNUserControl);
        }
    }
}
