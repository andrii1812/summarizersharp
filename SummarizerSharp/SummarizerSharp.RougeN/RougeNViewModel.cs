﻿using System.Collections.Generic;
using SummarizerSharp.Data;
using SummarizerSharp.Data.Core;

namespace SummarizerSharp.RougeN
{
    public class RougeNViewModel : ModuleBaseViewModel
    {
        private IList<QueueItem> _allModules;

        public RougeNViewModel(Module module) : base(module)
        {
            Config.StandardInputModule = InputModule;
        }

        public override IPluginConfiguration CurrentConfiguration {
            get { return base.CurrentConfiguration; }
            set
            {
                base.CurrentConfiguration = value;
                Config.Computed += ResultChanged;
            }
        }

        private void ResultChanged()
        {
            OnPropertyChanged(nameof(Result));
        }

        public RougeNConfig Config => CurrentConfiguration as RougeNConfig;

        public IList<QueueItem> AllModules
        {
            get { return _allModules; }
            set
            {
                _allModules = value;
                Config.StandardInputModule = InputModule;
                Configure();
            }
        }
        
        public QueueItem InputModule => AllModules?[StandardInputModuleIndex];

        public int StandardInputModuleIndex
        {
            get
            {
                return Config.StandardInputModuleIndex;
            }
            set
            {
                Config.StandardInputModuleIndex = value;
                Config.StandardInputModule = InputModule;
                OnChange();
            }
        }

        protected override void EditingEnded()
        {
            Config.Computed -= ResultChanged;
            Config.Computed += ResultChanged;
            base.EditingEnded();
            OnPropertyChanged(nameof(StandardInputModuleIndex));
            OnPropertyChanged(nameof(NGram));
        }

        public double Result => Config.Result;

        public int NGram
        {
            get { return Config.NGramLength; }
            set
            {
                Config.NGramLength = value;
                OnChange();
            }
        }
    }
}