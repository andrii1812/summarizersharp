﻿using SummarizerSharp.Data.Core;

namespace TextWriter
{
    public class TextWriterConfig : IPluginConfiguration
    {
        public string FileName { get; set; }
        public object Clone()
        {
            return new TextWriterConfig {FileName = FileName};
        }

        public bool Validate()
        {
            return FileName != "";
        }
    }
}