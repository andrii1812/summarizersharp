﻿using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;

namespace TextWriter
{
    /// <summary>
    /// Interaction logic for TextReaderUserControl.xaml
    /// </summary>
    public partial class TextWriterUserControl : UserControl
    {
        public TextWriterUserControl()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new SaveFileDialog();
            var result = dialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                var model = (TextWriterViewModel) DataContext;
                model.FileName = dialog.FileName;
            }
        }
    }
}
