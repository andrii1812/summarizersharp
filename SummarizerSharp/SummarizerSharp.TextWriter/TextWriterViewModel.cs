﻿using SummarizerSharp.Data;
using SummarizerSharp.Data.Core;

namespace TextWriter
{
    public class TextWriterViewModel : ModuleBaseViewModel
    {
        private TextWriterConfig Config => CurrentConfiguration as TextWriterConfig;

        public string FileName
        {
            get { return Config.FileName; }
            set
            {
                Config.FileName = value;
                OnPropertyChanged(nameof(FileName));
                OnChange();
            }
        }

        protected override void EditingEnded()
        {
            OnPropertyChanged(nameof(FileName));
            base.EditingEnded();
        }

        public TextWriterViewModel(Module module) : base(module)
        {
        }
    }
}