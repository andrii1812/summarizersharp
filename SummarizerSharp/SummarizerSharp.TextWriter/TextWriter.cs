﻿using System;
using System.IO;
using SummarizerSharp.Data.Core;
using TextWriter.Writers;
using TextWriter.Writers.Interfaces;

namespace TextWriter
{
    public class TextWriter : Module
    {
        private TextWriterConfig _configuration;
        private ITextWriter _writer;

        public override void Configure(IPluginConfiguration config)
        {
            base.Configure(config);
            _configuration = config as TextWriterConfig;
            if (_configuration == null)
            {
                throw new ArgumentNullException();
            }
            try
            {
                var fileName = _configuration.FileName;
                var ext = Path.GetExtension(fileName);
                switch (ext)
                {
                    case ".txt":
                        _writer = new TxtTextWriter(_configuration.FileName);
                        break;
                    case ".doc":
                    case ".docx":
                        _writer = new WordTextWriter(_configuration.FileName);
                        break;
                    case ".pdf":
                        _writer = new PdfTextWriter(_configuration.FileName);
                        break;
                    default:
                        throw new Exception();
                }
            }
            catch (Exception)
            {
                throw new InvalidOperationException("bad filename provided");
            }
        }

        public override IProcessingResult Process(IProcessingInput input)
        {
            var sent = input.Sentences;

            _writer.WriteToEnd(sent);
            
            return input.ToResult();
        }

        public override IPluginConfiguration GetConfiguration()
        {
            return new TextWriterConfig {FileName = ""};
        }

        public override Type GetViewModel()
        {
            return typeof(TextWriterViewModel);
        }

        public override Type GetUserControl()
        {
            return typeof(TextWriterUserControl);
        }
    }
}
