﻿using System.Collections.Generic;
using SharpNL.SentenceDetector;

namespace TextWriter.Writers.Interfaces
{
    interface ITextWriter
    {
        void WriteToEnd(IReadOnlyList<ISentence> content);
    }
}