﻿using System.Collections.Generic;
using System.IO;
using SharpNL.SentenceDetector;
using TextWriter.Writers.Interfaces;

namespace TextWriter.Writers
{
    class TxtTextWriter : ITextWriter
    {
        private readonly string _fileName;

        public TxtTextWriter(string fileName)
        {
            _fileName = fileName;
        }

        public void WriteToEnd(IReadOnlyList<ISentence> content)
        {
            int len = 0;
            using (StreamWriter writer = new StreamWriter(_fileName))
            {
                foreach (var sentence in content)
                {
                    writer.Write(sentence.Text + " ");
                    len += sentence.Length;
                    if (len > 120)
                    {
                        writer.Write('\n');
                        len = 0;
                    }
                }
            }
        }
    }
}